import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.less']
})
export class CvComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  employmentHead = ['Job title', 'Company name', 'Location', 'Dates of employment'];
  employmentElements: any = [
    {title: 'Intern Software Developer', name: 'Airsweb', location: 'Rainhill', dates: 'July 2018 – August 2019'},
    {title: 'Sandwich Artist', name: 'Subway', location: 'Warrington, Gemini', dates: '2016 - 2018'},
    {title: 'Catering and Hospitality Staff', name: 'St. Helens RFC', location: 'St. Helens', dates: '2015 - 2016'},
  ];

  qualsHead = ['2009 – 2014 The Sutton Academy', ' '];
  qualsElements: any = [
    {title: 'GCSE English (Language and Literature)', grade: 'C'},
    {title: 'GCSE Mathematics', grade: 'C'},
    {title: 'GCSE Design & Technology: Electronic Products', grade: 'B'},
    {title: 'BTEC ICT (equivalent to a GCSE grade A)', grade: 'Distinction'},
    {title: 'BTEC Science (equivalent to a GCSE grade C)', grade: 'Pass'},
  ];

  collegeHead1 = ['2014 – 2016 St Helens College, BTEC Level 3 Extended Diploma in IT', ' '];
  collegeElements1: any = [
    {title: 'Overall Grade', grade: 'D*D*D*'},
  ];
  collegeHead2 = ['Certificates Achieved'];
  collegeElements2: any = [
    {title: 'Microsoft Specialist - Word'},
    {title: 'Microsoft Specialist - PowerPoint'},
    {title: 'Microsoft Specialist – Excel'},
  ];

  uniHead = ['2016 – 2020 Liverpool John Moore’s University, Software Engineering', ' '];
  uniElements: any = [
    {title: 'Graduate Grade', grade: '2.1'},
  ];
}
