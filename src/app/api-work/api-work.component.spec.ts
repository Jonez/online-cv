import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApiWorkComponent } from './api-work.component';

describe('ApiWorkComponent', () => {
  let component: ApiWorkComponent;
  let fixture: ComponentFixture<ApiWorkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApiWorkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApiWorkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
