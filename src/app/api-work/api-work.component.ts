import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-api-work',
  templateUrl: './api-work.component.html',
  styleUrls: ['./api-work.component.less']
})
export class ApiWorkComponent implements OnInit {

  starwars:boolean;
  pokemon:boolean;

  constructor() { 
    this.starwars = true;
    this.pokemon = false;
  }

  ngOnInit(): void {
  }

  starwarsbtn(){
    this.starwars = true;
    this.pokemon = false;
  }

  pokemonbtn(){
    this.pokemon = true;
    this.starwars = false;
  }

}
